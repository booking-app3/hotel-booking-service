Find api key for admin at: http://localhost:8001/consumers/admin/key-auth

Play with docker steps:

1. create 3 instances
2. run docker swarm init on one of them
3. run the command from the previous output on the other ones
4. copy kong-stack.yml, poartainer-agent-stack.yml, kong, database on manager node
5. deploy app_stack
6. make requests at <ip-play-with-docker-manager>/...