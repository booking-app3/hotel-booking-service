CREATE DATABASE booking_db;
use booking_db;

CREATE TABLE City (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    city_name VARCHAR(30) NOT NULL
);

CREATE TABLE ComfortLevel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    comfort_level INTEGER NOT NULL
);

CREATE TABLE Facility (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    facility_name VARCHAR(30) NOT NULL,
    comf_level_id INTEGER NOT NULL,
    CONSTRAINT facility_comf_level FOREIGN KEY (comf_level_id)
    REFERENCES ComfortLevel (id)
    ON DELETE CASCADE
);

CREATE TABLE Hotel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hotel_name VARCHAR(30) NOT NULL,
    city_id INTEGER NOT NULL,
    stars INTEGER NOT NULL,
    CONSTRAINT hotel_city_id FOREIGN KEY (city_id)
    REFERENCES City (id)
    ON DELETE CASCADE
);

CREATE TABLE HotelComfortLevel (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    comf_level_id INTEGER NOT NULL,
    hotel_id INTEGER NOT NULL,
    CONSTRAINT hotel_comf_level FOREIGN KEY (comf_level_id)
    REFERENCES ComfortLevel (id)
    ON DELETE CASCADE,
    CONSTRAINT comf_level_hotel FOREIGN KEY (hotel_id)
    REFERENCES Hotel (id)
    ON DELETE CASCADE,
    CONSTRAINT Hotel_Comfort UNIQUE (hotel_id, comf_level_id)
);

CREATE TABLE Room (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    room_floor INTEGER NOT NULL,
    hotel_id INTEGER NOT NULL,
    max_pers INTEGER NOT NULL,
    description VARCHAR(100) NOT NULL,
    CONSTRAINT room_hotel_id FOREIGN KEY (hotel_id)
    REFERENCES Hotel (id)
    ON DELETE CASCADE
);

CREATE TABLE Guest (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(30) NOT NULL UNIQUE,
    password VARCHAR(512) NOT NULL,
    email VARCHAR(30) NOT NULL UNIQUE
);

CREATE TABLE Reservation (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    room_id INTEGER NOT NULL,
    guest_id INTEGER NOT NULL,
    check_in_date DATETIME NOT NULL,
    check_out_date DATETIME NOT NULL,
    CONSTRAINT reserv_room_id FOREIGN KEY (room_id)
    REFERENCES Room (id)
    ON DELETE CASCADE,
    CONSTRAINT reserv_guest_id FOREIGN KEY (guest_id)
    REFERENCES Guest (id)
    ON DELETE CASCADE
);


