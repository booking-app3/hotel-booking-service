# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]

## 2021-05-05

### Added
- Initial commit

## 2021-05-06

### Added
- Add init.sql to create database
- Add docker-compose file for testing
- Add database and adminer images to docker-compose
- Integrate portainer stack

### Updated
- Move to docker swarm and create kong-stack.yml
- Move management.yml and docker-compose.yml contents to kong-stack.yml along with necessary dependencies
- Use GitLab registry image for management service

### Fixed
- Database type

## 2021-05-07
### Added
- Add kong image to kong-stack.yml and kong.yml as declarative file - not working yet

## 2021-05-09
### Added
- Add reservation service to kong-stack.yml

## 2021-05-12
### Added
- Add authorization for accessing services based on hey-auth and acl: basic user can only access the Reservation service and admin can access everything

### Fixed
- Fix kong paths and services